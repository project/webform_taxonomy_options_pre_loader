Introduction
-----------
This module provides the facility to use taxonomy term as webform preload list.

Requirements
------------
Drupal 7.x
Webform 7.x

Installation
------------
1. Copy the entire webform_taxonomy_options_pre_loader directory
   the Drupal sites/all/modules directory.

2. Login as an administrator. 
   Enable the module in the "Administrator" -> "Modules"

How to use:
-----------
1.Login as admin to install.
2.Go to the webform and and edit any select list.
3.There you will see the taxonomy terms to use.
